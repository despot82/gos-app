var express = require("express");
var app = express();
var path = require("path");
var server = require("http").createServer(app);
var io = require("socket.io")(server);
var cors = require("cors");
var port = process.env.PORT || 3000;
let UserModel = require("./models/UserModel");
let MessageModel = require("./models/MessageModel");

var routes = require("./routes/routes");

server.listen(port, () => {
    console.log("Server listening at port %d", port);
});

app.use(cors());
// Routing
app.use(express.static(path.join(__dirname, "public")));
routes.assignRoutes(app); // load routes for model access

// Chatroom
var numUsers = 0;

io.on("connection", (socket) => {
    console.log("we got connection!!!!");
    var addedUser = false;

    // when the client emits 'add user', this listens and executes
    socket.on("new user", (username) => {
        console.log("In socke.on new user !!!!");
        if (addedUser) return;
        // store user in mongodb
        let msg = new UserModel({
            name: username,
            socket: socket.id,
            password: "abc"
        });

        msg.save()
            .then((doc) => {
                console.log(doc);
            })
            .catch((err) => {
                console.error(err);
                // check if user exists, so update the socketId because on every disconect/connect op the socketId changes
                if (err.code == 11000) {
                    // user exists. So update
                    UserModel.findOne(
                        {
                            name: username
                        },
                        (err, doc) => {
                            doc.socket = socket.id;
                            doc.save();
                        }
                    );
                }
            });
    });

    socket.on("private message", (data) => {
        console.log("private recieved!");
        //get from, to and the new message, save on db and emit event to update message list
        var socketToId = data.socket;
        var message = data.message;
        console.log(socketToId, message);
        var fromUser = message.fromUser;
        var toUser = message.toUser;
        var msg = message.message;
        var date = new Date();
        MessageModel.findOne({ from: fromUser, to: toUser }, (err, doc) => {
            if (!err) {
                console.log(doc);
                // if Message doc exists update
                if (doc) {
                    console.log("updating...");
                    MessageModel.update({ _id: doc._id }, { $push: { messages: { message: msg, date: date } } })
                        .then((doc) => {
                            console.log("updated!", doc);
                        })
                        .catch((err) => {
                            console.log("error upd", err);
                        });
                } else {
                    // save new Message doc
                    var msgSave = new MessageModel({
                        from: fromUser,
                        to: toUser,
                        messages: [{ message: msg, date: date }]
                    });
                    msgSave
                        .save()
                        .then((doc) => {
                            console.log("first message saved!", doc);
                        })
                        .catch((err) => {
                            console.log("error saving first msg ", err);
                        });
                }
            } else {
                console.log(err);
            }
        });
        socket.to(socketToId).emit("private message", message);
    });

    socket.on("private message img", (data) => {
        console.log("private img recieved!");
        //get from, to and the new message, save on db and emit event to update message list
        var socketToId = data.socket;
        var message = data.message; // arrayBuffer
        console.log(socketToId, message);
        socket.to(socketToId).emit("private message img", message);
    });

    // when the users login
    socket.on("connected user", (user) => {
        socket.broadcast.emit("connected user", {
            username: user
        });
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on("typing", () => {
        socket.broadcast.emit("typing", {
            username: socket.username
        });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on("stop typing", () => {
        socket.broadcast.emit("stop typing", {
            username: socket.username
        });
    });

    // when the user disconnects.. perform this
    socket.on("disconnect", () => {
        if (addedUser) {
            --numUsers;
            // delete user from database
            var username = socket.username;
            UserModel.findOneAndRemove({
                name: username
            })
                .then((response) => {
                    console.log(response);
                })
                .catch((err) => {
                    console.error(err);
                });
            // echo globally that this client has left
            socket.broadcast.emit("user left", {
                username: socket.username,
                numUsers: numUsers
            });
        }
    });
});
