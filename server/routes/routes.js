var users = require('./users');
var msgs = require('./messages');

exports.assignRoutes = function (app) {
    app.get('/users', users.getUsers);
    app.get('/messages', msgs.getMessages);
}
