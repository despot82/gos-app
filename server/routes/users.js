var usersDomain = require('./../domain/users');

exports.getUsers = function(req, res, next) {
  var userData = req.body;
  var userName = req.param('user');
  if (userName) {
    usersDomain.findByUserName(userName)
      .then(user => {
        res.send(user)
      })
      .catch(err => {
        res.status(400).send(err);
      })
  } else {
    usersDomain.getUsers(userData)
      .then(user => {
        res.send(user)
      })
      .catch(err => {
        res.status(400).send(err);
      })
  }

}
