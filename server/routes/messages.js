var messageDomain = require('./../domain/messages');

exports.getMessages = function (req, res, next) {
    var from = req.param('from');
    var to = req.param('to');
    if (from && to) {
        messageDomain.getMessages(from, to)
            .then(msg => {
                res.send(msg)
            })
            .catch(err => {
                res.status(400).send(err);
            })
    }

}
