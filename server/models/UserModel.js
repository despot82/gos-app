const mongoose = require("../config/database")
const validator = require('validator')
var Schema = mongoose.Schema;

const schema = {
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    required: true,
    validate: (value) => {
      return !validator.isEmpty(value)
    },
    createIndexes: true
  },
  socket: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true,
    select: false
  }
};
const collectionName = "user"; // Name of the collection of documents
const userSchema = new Schema(schema);
const User = mongoose.model(collectionName, userSchema);

module.exports = User;
