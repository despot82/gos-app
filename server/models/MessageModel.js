const mongoose = require("../config/database")
const validator = require('validator')
var Schema = mongoose.Schema;

// modeling subdocument message
const messageTypeSchema = new Schema({
  message: {
    type: String,
    required: true
  },
  date: {
    type: mongoose.SchemaTypes.Date,
    required: true
  }
})

// main schema message
const schema = {
  from: {
    type: String,
    required: true,
    lowercase: true,
    validate: (value) => {
      return !validator.isEmpty(value)
    },
    createIndexes: true
  },
  to: {
    type: String,
    required: true,
    lowercase: true,
    validate: (value) => {
      return !validator.isEmpty(value)
    },
    createIndexes: true
  },
  messages: [messageTypeSchema]
};

const collectionName = "message"; // Name of the collection of documents
const messageSchema = new Schema(schema);
const Message = mongoose.model(collectionName, messageSchema);

module.exports = Message;
