let MessageModel = require("./../models/MessageModel");

exports.getMessages = function (from, to) {
    return new Promise(function (resolve, reject) {
        // find all messages from and to users
        MessageModel.find({$or: [{from: from, to: to}, {from: to, to: from}]}, function (err, msgs) {
            if (!err) {
                // order by date field
                var listMsgs = [];
                for (let i = 0; i < msgs.length; i++) {
                    var msgObj = msgs[i];
                    console.log(msgObj);

                    for (let j = 0; j < msgObj.messages.length; j++) {
                        var msgObjList = msgObj.messages[j].toObject();

                        console.log(msgObj.from, msgObj.to)
                        msgObjList.fromUser = msgObj.from;
                        msgObjList.toUser = msgObj.to;
                        listMsgs.push(msgObjList)
                    }
                }
                listMsgs.sort(function(a,b){
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    return new Date(a.date) - new Date(b.date);
                });
                resolve(listMsgs);
            } else {
                reject(err);
            }
        })
    });
};

