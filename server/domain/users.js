let UserModel = require("./../models/UserModel");

exports.getUsers = function() {
  return new Promise(function(resolve, reject) {
    // find all
    UserModel.find({}, function(err, users) {
      if (!err) {
        resolve(users);
      } else {
        reject(err);
      }
    })
  });
};
exports.findByUserName = function(name) {
  return new Promise(function(resolve, reject) {
    // find all
    UserModel.findOne({
      name: name
    }, function(err, users) {
      if (!err) {
        resolve(users);
      } else {
        reject(err);
      }
    })
  });
};
